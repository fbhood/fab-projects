<?php
function fabpr_projects_setup_post_type()
{
    // register our custom post-types and taxonomies here
    include('post-types/project.php');
    include('taxonomies/project-category.php');
}
add_action('init', 'fabpr_projects_setup_post_type');

function fabpr_projects_install()
{
    // trigger our function that registers the custom post type
    fabpr_projects_setup_post_type();

    // clear the permalinks after the post type has been registered
    flush_rewrite_rules();
}
register_activation_hook(__FILE__, 'fabpr_projects_install');

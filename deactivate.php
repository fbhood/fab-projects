<?php
function fabpr_projects_deactivation()
{
    // unregister the post type, so the rules are no longer in memory
    unregister_post_type('project');
    unregister_taxonomy('project-category');
    // clear the permalinks to remove our post type's rules from the database
    flush_rewrite_rules();
}
register_deactivation_hook(__FILE__, 'fabpr_projects_deactivation');

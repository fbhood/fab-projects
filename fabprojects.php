<?php

/**
 * Plugin Name:     Fab Projects
 * Plugin URI:      https://plugins.fabiopacifici.com/fab-projects
 * Description:     Add Projects to your WordPress installation.
 * Author:          Fabio Pacifici
 * Author URI:      https://fabiopacifici.com
 * Text Domain:     fp_projects
 * Domain Path:     /languages
 * Version:         1.1.0
 *
 * @package         Fab Projects
 */

// Your code starts here.
include('activate.php'); // 1. contains post-types and taxo register function - include post types inside
include('deactivate.php'); //2. contain post-types and taxo deregistration  - include taxonomies inside

include('blocks/projects.php');
include('blocks/project.php');

include('metaboxes/projectClass.php');

// Custom Post type actions and filters
add_action('init', 'fabpr_projects_init');
add_filter('post_updated_messages', 'fabpr_projects_updated_messages');


// Cutom taxo actions and filters
add_action('init', 'fabpr_projects_category_init');
add_filter('term_updated_messages', 'fabpr_projects_category_updated_messages');


/* Filter the single_template with our custom function*/
add_filter('single_template', 'fabpr_projects_single_project_template');


function fabpr_projects_single_project_template($single)
{

    global $post;
    $currentTheme = wp_get_theme();

    /* Checks for single template by post type */
    if ($post->post_type == 'project') {

        /* Checks if the current theme is the Twenty Twenty */
        if ($currentTheme == "Twenty Twenty") {
            if (file_exists(plugin_dir_path(__FILE__) . 'templates/2020/single-project.php')) {
                return  plugin_dir_path(__FILE__) . 'templates/2020/single-project.php';
            } /* Checks if the current theme is the Twenty Nineteen */
        } elseif ($currentTheme == "Twenty Nineteen") {

            if (file_exists(plugin_dir_path(__FILE__) . 'templates/2019/single-project.php')) {
                return  plugin_dir_path(__FILE__) . 'templates/2019/single-project.php';
            }/* Checks if the current theme is the Twenty Seventeen of his child */
        } elseif ($currentTheme == "Twenty Seventeen" || get_template_directory() !== get_stylesheet_directory()) {

            if (file_exists(plugin_dir_path(__FILE__) . 'templates/2017/single-project.php')) {
                return  plugin_dir_path(__FILE__) . 'templates/2017/single-project.php';
            }
        } else {
            if (file_exists(plugin_dir_path(__FILE__) . 'templates/single-project.php')) {
                return  plugin_dir_path(__FILE__) . 'templates/single-project.php';
            }
        }
    }

    return $single;
}

<?php

/**
 * Registers the `project` post type.
 */
function fabpr_projects_init()
{
	register_post_type('project', array(
		'labels'                => array(
			'name'                  => __('Projects', 'fp_projects'),
			'singular_name'         => __('Project', 'fp_projects'),
			'all_items'             => __('All Projects', 'fp_projects'),
			'archives'              => __('Project Archives', 'fp_projects'),
			'attributes'            => __('Project Attributes', 'fp_projects'),
			'insert_into_item'      => __('Insert into Project', 'fp_projects'),
			'uploaded_to_this_item' => __('Uploaded to this Project', 'fp_projects'),
			'featured_image'        => _x('Featured Image', 'project', 'fp_projects'),
			'set_featured_image'    => _x('Set featured image', 'project', 'fp_projects'),
			'remove_featured_image' => _x('Remove featured image', 'project', 'fp_projects'),
			'use_featured_image'    => _x('Use as featured image', 'project', 'fp_projects'),
			'filter_items_list'     => __('Filter Projects list', 'fp_projects'),
			'items_list_navigation' => __('Projects list navigation', 'fp_projects'),
			'items_list'            => __('Projects list', 'fp_projects'),
			'new_item'              => __('New Project', 'fp_projects'),
			'add_new'               => __('Add New', 'fp_projects'),
			'add_new_item'          => __('Add New Project', 'fp_projects'),
			'edit_item'             => __('Edit Project', 'fp_projects'),
			'view_item'             => __('View Project', 'fp_projects'),
			'view_items'            => __('View Projects', 'fp_projects'),
			'search_items'          => __('Search Projects', 'fp_projects'),
			'not_found'             => __('No Projects found', 'fp_projects'),
			'not_found_in_trash'    => __('No Projects found in trash', 'fp_projects'),
			'parent_item_colon'     => __('Parent Project:', 'fp_projects'),
			'menu_name'             => __('Projects', 'fp_projects'),
		),
		'public'                => true,
		'hierarchical'          => false,
		'show_ui'               => true,
		'show_in_nav_menus'     => true,
		'supports'              => array('title', 'editor', 'thumbnail'),
		'has_archive'           => true,
		'rewrite'               => true,
		'query_var'             => true,
		'menu_icon'             => 'dashicons-clipboard',
		'show_in_rest'          => true,
		'rest_base'             => 'project',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	));
}

/**
 * Sets the post updated messages for the `project` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `project` post type.
 */
function fabpr_projects_updated_messages($messages)
{
	global $post;

	$permalink = get_permalink($post);

	$messages['project'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf(__('Project updated. <a target="_blank" href="%s">View Project</a>', 'fp_projects'), esc_url($permalink)),
		2  => __('Custom field updated.', 'fp_projects'),
		3  => __('Custom field deleted.', 'fp_projects'),
		4  => __('Project updated.', 'fp_projects'),
		/* translators: %s: date and time of the revision */
		5  => isset($_GET['revision']) ? sprintf(__('Project restored to revision from %s', 'fp_projects'), wp_post_revision_title((int) $_GET['revision'], false)) : false,
		/* translators: %s: post permalink */
		6  => sprintf(__('Project published. <a href="%s">View Project</a>', 'fp_projects'), esc_url($permalink)),
		7  => __('Project saved.', 'fp_projects'),
		/* translators: %s: post permalink */
		8  => sprintf(__('Project submitted. <a target="_blank" href="%s">Preview Project</a>', 'fp_projects'), esc_url(add_query_arg('preview', 'true', $permalink))),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf(
			__('Project scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Project</a>', 'fp_projects'),
			date_i18n(__('M j, Y @ G:i'), strtotime($post->post_date)),
			esc_url($permalink)
		),
		/* translators: %s: post permalink */
		10 => sprintf(__('Project draft updated. <a target="_blank" href="%s">Preview Project</a>', 'fp_projects'), esc_url(add_query_arg('preview', 'true', $permalink))),
	);

	return $messages;
}

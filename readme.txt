=== Fab Projects for WordPress === 
Contributors: fabsere
Donate link: https://fabiopacifici.com/product/open-source-donate/
Tags: projects, project-category,
Requires at least: 4.5
Tested up to: 5.5
Stable tag: 1.1.0
Requires PHP: 7.2
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

This plugin adds a project menu item to the WordPress admin panel. It lets the user create e project and assign a goal to it.


== Description ==

The Projects Plugin for WordPress introcudes a new "Project" menu item to the WordPress Admin panel. Projects can be organized
in categories and each project can have a goal that is shown to users on the website.


Features of the plugin:

*   Adds Projects to the WordPress Admin Panel
*   Users can organize projects in categories
*   Users can set a goal for the project.
*   Tested up to WordPress 5.5
*   Designed to work with any Official or unofficial theme.


== Installation ==

This section describes how to install the plugin and get it working.

1. Upload `projects` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress


== Frequently Asked Questions ==

= Does it work with the new WordPress 2020 theme? =

Yes. This plugin is compatible with the WordPress Twenty Twenty theme.
The theme has been tested with the last five official WordPress themes and 
should be compatible with any other theme.

= What about Gutemberg? =

The plugin works with the Gutemberg editor. However, project Blocks are not yet available, they will be introduced in a future release of the plugin.

== Screenshots ==

1. Admin Panel: Add a new Project post
2. Admin Panel: All Project posts
3. Admin Panel: Projects categories
4. Menu: Add Projects menu items
5. Twenty Twenty Theme: Project Page
6. Twenty Twenty Theme: Projects Archive Page
7. Twenty Nineteen Theme: Project Page 
8. Twenty Seventeen Theme: Project Page 


== Changelog ==

= 1.1.0 =
Release date: 17 August 2020
- replace get_current_theme deprecated function with wp_get_theme
- update version number to 1.1.0 
- update tested up to 5.5


= 1.0 =
First Stable Release

== Upgrade Notice ==

= 1.0 =
First Stable Release

== Compatible with the lastest Official Themes ==

This plugin is fully compatible with the latest official WordPress Themes from Twenty Fiftheen up to Twenty Twenty.

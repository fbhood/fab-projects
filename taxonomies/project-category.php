<?php

/**
 * Registers the `project_category` taxonomy,
 * for use with 'project'.
 */
function fabpr_projects_category_init()
{
	register_taxonomy('project-category', array('project'), array(
		'hierarchical'      => false,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => false,
		'query_var'         => true,
		'rewrite'           => true,
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts',
		),
		'labels'            => array(
			'name'                       => __('Categories', 'wpfab'),
			'singular_name'              => _x('Category', 'taxonomy general name', 'wpfab'),
			'search_items'               => __('Search Categories', 'wpfab'),
			'popular_items'              => __('Popular Categories', 'wpfab'),
			'all_items'                  => __('All Categories', 'wpfab'),
			'parent_item'                => __('Parent Category', 'wpfab'),
			'parent_item_colon'          => __('Parent Category:', 'wpfab'),
			'edit_item'                  => __('Edit Category', 'wpfab'),
			'update_item'                => __('Update Category', 'wpfab'),
			'view_item'                  => __('View Category', 'wpfab'),
			'add_new_item'               => __('Add New Category', 'wpfab'),
			'new_item_name'              => __('New Category', 'wpfab'),
			'separate_items_with_commas' => __('Separate Categories with commas', 'wpfab'),
			'add_or_remove_items'        => __('Add or remove Categories', 'wpfab'),
			'choose_from_most_used'      => __('Choose from the most used Categories', 'wpfab'),
			'not_found'                  => __('No Categories found.', 'wpfab'),
			'no_terms'                   => __('No Categories', 'wpfab'),
			'menu_name'                  => __('Categories', 'wpfab'),
			'items_list_navigation'      => __('Categories list navigation', 'wpfab'),
			'items_list'                 => __('Categories list', 'wpfab'),
			'most_used'                  => _x('Most Used', 'project-category', 'wpfab'),
			'back_to_items'              => __('&larr; Back to Categories', 'wpfab'),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'project-category',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	));
}

/**
 * Sets the post updated messages for the `project_category` taxonomy.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `project_category` taxonomy.
 */
function fabpr_projects_category_updated_messages($messages)
{

	$messages['project-category'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => __('Category added.', 'wpfab'),
		2 => __('Category deleted.', 'wpfab'),
		3 => __('Category updated.', 'wpfab'),
		4 => __('Category not added.', 'wpfab'),
		5 => __('Category not updated.', 'wpfab'),
		6 => __('Categories deleted.', 'wpfab'),
	);

	return $messages;
}

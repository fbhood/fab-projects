<?php

/**
 * The template for displaying all single project posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
?>

<div id="primary" class="content-area">
    <main id="main" class="site-main">

        <?php

        /* Start the Loop */
        while (have_posts()) :
            the_post();

            include('template-parts/content.php');

            if (is_singular('project')) {
                // Previous/next post navigation.
                the_post_navigation(
                    array(
                        'next_text' => '<span class="meta-nav" aria-hidden="true">' . __('Next Post', 'fp_projects') . '</span> ' .
                            '<span class="screen-reader-text">' . __('Next post:', 'fp_projects') . '</span> <br/>' .
                            '<span class="post-title">%title</span>',
                        'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __('Previous Post', 'fp_projects') . '</span> ' .
                            '<span class="screen-reader-text">' . __('Previous post:', 'fp_projects') . '</span> <br/>' .
                            '<span class="post-title">%title</span>',
                    )
                );
            }

            // If comments are open or we have at least one comment, load up the comment template.
            if (comments_open() || get_comments_number()) {
                comments_template();
            }

        endwhile; // End of the loop.
        ?>

    </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();

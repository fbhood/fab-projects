<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main" style="padding-left: 2rem; padding-right: 2rem;">

			<?php
			/* Start the Loop */
			while (have_posts()) : the_post();
				if (has_post_thumbnail()) {
					the_post_thumbnail('full');
				}
				the_title();
				the_content();
				?>
				<strong><?php _e('Project Goal', 'fp_projects'); ?>:</strong>

				<?php $meta = get_post_meta($post->ID, '_project_goal_meta_key', true);
					echo $meta;
					?>

			<?php
				// If comments are open or we have at least one comment, load up the comment template.
				if (comments_open() || get_comments_number()) :
					comments_template();
				endif;

				the_post_navigation();

			endwhile; // End of the loop.
			?>
		</main><!-- #main -->
	</div><!-- #primary -->
	<?php get_sidebar(); ?>
</div><!-- .wrap -->

<?php get_footer();
